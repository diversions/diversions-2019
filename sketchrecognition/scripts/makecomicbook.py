"""
translates jsonstream into text for pdf rendering

usage:

cat mim.pub.jsons | python3 makecomicbook.py | python pdf.py
"""

import sys, json, os
count = 0
for line in sys.stdin:
	data = json.loads(line)
	print ("{0} {1}".format(data['dating'], data['geography']))
	for c in data['contours']:
		print ("./mim/{0}".format(c['path']))
		plabel, pcert = c['sketch_recognizer'][0]
		pcert *= 100
		if pcert > 50:
			cert = "!"
		else:
			cert = "?"
		label = "{0}{1}".format(plabel, cert)
		print ("short:{0}".format(label))
	base, _ = os.path.splitext(data['image'])
	print ("./mim/{0}.trim.jpg".format(base))
	print ("caption: {0}".format(data['objectName'] or data['inventoryNb']))
	count +=1
	if count == 50:
		break