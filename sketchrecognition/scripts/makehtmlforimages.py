#!/usr/bin/env python3

import argparse
import sys, os, json
from csv import DictReader
from PIL import Image 


ap = argparse.ArgumentParser("")
# ap.add_argument("input", nargs="+")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
args = ap.parse_args()

# inputs = sorted(args.input)
# for n in inputs:
#	print (n)
count, exists_count = 0, 0
for row in DictReader(args.input):
	# print (row)
	image = row['image']
	im, ext = os.path.splitext(image)
	orig = os.path.join(".", im+".orig.jpg")
	trim = os.path.join(".", im+".trim.jpg")
	contours = os.path.join(".", im+".contours.png")
	exists = os.path.exists(orig)
	count +=1
	if exists:
		exists_count += 1
	# print (row['objectName'], image, orig, os.path.exists(orig))
	nd = {}
	nd['orig'] = orig
	nd['trim'] = trim
	nd['contours'] = contours
	nd['objectName'] = row['objectName']
	im = Image.open(trim)
	nd['width'], nd['height'] = im.size
	print (json.dumps(nd))

print ("{0}/{1} found".format(exists_count, count), file=sys.stderr)

