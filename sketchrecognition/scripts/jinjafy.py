#!/usr/bin/env python3

import argparse, sys, json, time, os
from jinja2 import Template, DictLoader, Environment, FileSystemLoader


def JSONStream (file):
    """ generator that returns one parsed json object per line of a file"""
    for line in file:
        yield json.loads(line)

ap = argparse.ArgumentParser("jinjafy!")
ap.add_argument("template")
ap.add_argument("--columns", type=int, default=None, help="treat incoming data as text in this many columns and not json (the default behaviour)")
ap.add_argument("--data", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--listkey", default="items", help="if incoming data is a list, give it this name for the template, default: items")
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
ap.add_argument("--stream", action="store_true", default=False)
args = ap.parse_args()
if args.columns:
    data = []
    for line in args.data:
        line = line.split(None, args.columns-1)
        if line:
            data.append(line)
else:
    if not args.stream:
        data = json.load(args.data)
    else:
        data = { args.listkey: JSONStream(args.data) }

tpath, tname = os.path.split(args.template)
env = Environment(loader=FileSystemLoader(tpath))
import jinjafilters
for name, fn in jinjafilters.all.items():
    env.filters[name] = fn

template = env.get_template(tname)
if type(data) == list:
    # print ("Detected list, adding as {0}".format(args.listkey), file=sys.stderr)
    data = {
        args.listkey: data
    }
print (template.render(**data), file=args.output)
