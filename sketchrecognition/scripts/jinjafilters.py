import os


def basename (x):
    return os.path.splitext(x)[0]

all = {
    'basename': basename
}
