from xml.etree import ElementTree as ET 
import subprocess
import argparse
import os, sys

def render_svg (svgsrc, topath):
	p = subprocess.Popen(["inkscape", "--export-png="+topath, "--export-background=#000000FF", "--file=/dev/stdin"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = p.communicate(svgsrc.encode("utf-8"))
	return stdout, stderr

def process (svgpath):
	t = ET.parse(svgpath)
	svg = t.getroot()
	elts = list(t.findall("{http://www.w3.org/2000/svg}polyline"))
	for elt in elts:
		svg.remove(elt)
	ET.register_namespace("", "http://www.w3.org/2000/svg")
	newsvg = ET.tostring(svg, encoding="unicode")
	frame = 0
	base,ext = os.path.splitext(svgpath)
	output = {}
	output['contours'] = svgpath
	output['images'] = images = []
	for i, elt in enumerate(elts):
		frame += 1
		elt.attrib['style'] = "fill:none;stroke:rgb(0,0,0);stroke-width:3"
		svg.append(elt)
		newsvg = ET.tostring(svg, encoding="unicode")
		path = "{0}.{1:04d}.png".format(base,frame)
		print ("outputting {0}".format(path), file=sys.stderr)
		images.append(path)
		render_svg(newsvg, path)
	return output

if __name__ == "__main__":
	import sys, json
	for line in sys.stdin:
		data = json.loads(line)
		path = data['image']
		base, ext = os.path.splitext(path)
		data['contours_svg'] = contour_svg = base + ".contours.svg"
		print ("processing {0}".format(contour_svg), file=sys.stderr)
		results = process(contour_svg)
		data['contours_count'] = len(results["images"])
		data['contours'] = [{'path': x} for x in results["images"]]
		print (json.dumps(data))

