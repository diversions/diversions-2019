#!/usr/bin/env/ python
# encoding=utf8 

'''
For each of the pagenames, this script gets all the links mentioned in that page, and this in 4 different languages.
This was necessary to get some idea of the different species represented on Wikipedia.
!!!!!!!!!!!!!!!!!! ADD LANGUAGES in SCRIPT! MAKE FUNCTION!
'''

from bs4 import BeautifulSoup as bs
import requests

pages = ["Trees_of_Africa", "Trees_of_Africa&pagefrom=Senegalia+ataxacantha#mw-pages",\
"Trees_of_Algeria", "Trees_of_Angola", "Trees_of_Botswana", "Trees_of_the_Democratic_Republic_of_the_Congo",\
"Trees_of_Ethiopia", "Trees_of_Ghana", "Trees_of_Madagascar", "Trees_of_Morocco",\
"Trees_of_R%C3%A9union", "Trees_of_Seychelles", "Trees_of_South_Africa"]

for page in pages:
	wikipage = "https://en.wikipedia.org/wiki/"+page
	print("\npage:", wikipage)
	res = requests.get(wikipage)
	if res:

		soup = bs(res.text, "html.parser")
		print('soup:', soup)
		species = []
		for link in soup.find_all("a"):
			url = link.get("href", "")
			print('url:', url)
			if "/wiki/" in url:
				name_species = url.replace("/wiki/", "")
			species.append(name_species)

			destination = page+".txt"
			with open(destination, 'w') as source:
					for specie in species:
						source.write(specie)
						source.write('\n')
	else:
		pass

# complete_links =["https://en.wikipedia.org/wiki/", "https://es.wikipedia.org/wiki/", "https://fr.wikipedia.org/wiki/", "https://nl.wikipedia.org/wiki/"]

'''
comments:
Trees of Africa refer to all countries listed here: https://en.wikipedia.org/wiki/Ecoregions_of_Africa

'''