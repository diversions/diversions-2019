#!/usr/bin/env/ python
# encoding=utf8 

'''
This script takes a list of names and different languages for which it scrapes the content of the pages and saves it in separate textfiles.
'''

import wikipediaapi

species = ["Cinchona officinalis", "Juglans_nigra", "Elaeis guineensis", "Myristica fragrans", "Passiflora ligularis", "Eucalyptus regnans"]
languages = ['en', 'es', 'fr', 'nl']
languages_all = ['en', 'es', 'fr', 'nl', 'ar', 'ast', 'azb', 'az', 'ca', 'ceb', 'cs', 'csb', 'da', 'de', 'el', 'eu', 'eo', 'fa', \
'fi', 'frr', 'gd', 'gl', 'hi', 'hsb', 'ht', 'hu', 'it', 'ja', 'kk', 'ko', 'la', 'ln', 'lt', 'ms', 'nn', 'no', \
'oc', 'pl', 'pt', 'qu', 'ru', 'sa', 'sd', 'sh', 'simple', 'sk', 'su'\
'sv', 'th', 'tr', 'uk', 'vi', 'war', 'zh-yue','zh']

def gettext(species, languages):
	for specie in species: 
		for language in languages:
			print(specie)
			wiki_wiki = wikipediaapi.Wikipedia(
					language=language,
					extract_format=wikipediaapi.ExtractFormat.WIKI)

			text = ""
			p_wiki = wiki_wiki.page(specie)
			text = p_wiki.text
			length = len(text)
			title = specie + "_"+ language + ".txt"
			print('title:', title)
			print('length:', length)

			if text: 
				with open(title, 'w') as destination:
					destination.write(str(length))
					destination.write("\n\n")
					destination.write(text)

result = gettext(species, languages)

	