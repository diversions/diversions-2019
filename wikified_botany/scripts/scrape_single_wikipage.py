#!/usr/bin/env/ python
# encoding=utf8 

'''
This script takes a list of names and different languages for which it scrapes the content of the pages and saves it in separate textfiles.
'''

import wikipediaapi

species = "Passiflora ligularis"
languages = ['hr', 'su']
# languages = ['en', 'es', 'fr', 'nl']
# languages = ['bn', 'kk', 'csb', 'fa', 'gd', 'hi']
# languages = ['hr', 'hsb', 'ht', 'ja', 'kn', 'lt' ] 
# languages = ['ml', 'nn', 'oc', 'sa', 'sd', 'sh', 'ur']

def gettext(species, languages):
	for language in languages:
			print(species)
			wiki_wiki = wikipediaapi.Wikipedia(
					language=language,
					extract_format=wikipediaapi.ExtractFormat.WIKI)

			text = ""
			p_wiki = wiki_wiki.page(species)
			text = p_wiki.text
			length = len(text)
			title = species + "_"+ language + ".txt"
			print('title:', title)
			print('length:', length)

			if text: 
				with open(title, 'w') as destination:
					destination.write(str(length))
					destination.write("\n\n")
					destination.write(text)

result = gettext(species, languages)

	