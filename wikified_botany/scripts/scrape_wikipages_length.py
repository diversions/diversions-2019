#!/usr/bin/env/ python
# encoding=utf8 

'''
This script takes a list of names and different languages for which it scrapes the content of the pages and saves it in separate textfiles.
'''

import wikipediaapi
import csv

species = ["Cinchona officinalis", "Juglans_nigra", "Elaeis guineensis", "Myristica fragrans", "Passiflora ligularis", "Eucalyptus regnans"]
# Because Wikipedia allows only a limited set of queries, it helps to launch the script several times for different languages
# languages = ['en', 'es', 'fr', 'nl']
# languages = ['ar', 'ast', 'azb', 'az']
# languages = ['ca', 'ceb', 'cs', 'csb']
# languages = ['da', 'de', 'el', 'eu']
# languages = ['eo', 'fa', 'fi', 'frr']
# languages = ['gd', 'gl', 'hi', 'hsb']
# languages = ['ht', 'hu', 'it', 'ja']
# languages = ['kk', 'ko', 'la', 'ln']
# languages = ['lt', 'ms', 'nn', 'no']
# languages = ['oc', 'pl', 'pt', 'qu']
# languages = ['ru', 'sa', 'sd', 'sh']
# languages = ['simple', 'sk', 'su', 'sv']
# languages = ['th', 'tr', 'uk', 'vi']
languages = ['war', 'zh-yue','zh']

def gettext(species, languages):
	for specie in species: 
		for language in languages:
			if language: 
				wiki_wiki = wikipediaapi.Wikipedia(
						language=language,
						extract_format=wikipediaapi.ExtractFormat.WIKI)

				text = ""
				p_wiki = wiki_wiki.page(specie)
				text = p_wiki.text
				length = len(text)
				print('specie:', specie)
				print('language:', language)
				print('length:', length)

				with open('length_file.csv', mode='a') as length_file:
					length_writer = csv.writer(length_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
					length_writer.writerow([specie, language, length])

result = gettext(species, languages)

	