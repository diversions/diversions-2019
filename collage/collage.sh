#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'you need a keyword, a limit and a folder!'
    exit 0
fi

python3 scrape.py --keyword $1 --limit $2 --imagepath $1

for file in $1/*.jpg
do convert $file -gravity south -chop 0x25 $file
done

for file in $1/*.jpg
do python3 contours.py $file .
done

python3 siftjoin.py $1