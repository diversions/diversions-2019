#!/usr/bin/python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import math
import sys

imfile=(sys.argv[1])

img = cv2.imread(imfile)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ww, hh = img.shape[:2]

if( (gray[2,2]>150) or (gray[ww-2,hh-2]>150) ):
	gray2=255-gray
else:
	gray2=gray
# img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# plt.imshow(img)
# Prepocess
blur = cv2.GaussianBlur(gray2,(1,1),1000)
flag, thresh = cv2.threshold(blur, 90, 255, cv2.THRESH_BINARY)
# Find contours
_,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
contours = sorted(contours, key=cv2.contourArea,reverse=True) 

# change threshold when the contour is basically the whole image..

if( ( cv2.contourArea(contours[0]) /(ww*hh)) > 0.9 ):
	flag, thresh = cv2.threshold(blur, 110, 255, cv2.THRESH_BINARY)
	# Find contours
	_,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	contours = sorted(contours, key=cv2.contourArea,reverse=True) 

# change threshold when the contour is basically the whole image..

if( ( cv2.contourArea(contours[0]) /(ww*hh)) > 0.9 ):
	flag, thresh = cv2.threshold(blur, 130, 255, cv2.THRESH_BINARY)
	# Find contours
	_,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	contours = sorted(contours, key=cv2.contourArea,reverse=True) 

print( cv2.contourArea(contours[0]) / (ww * hh) )

# Select long perimeters only

perimeters = [cv2.arcLength(contours[i],True) for i in range(len(contours))]

listindex=[i for i in range(15) if perimeters[i]>perimeters[0]/2]
numcards=len(listindex)

mask = np.zeros_like(gray) # Create mask where white is what we want, black otherwise
# cv2.drawContours(mask, contours, idx, 255, -1) # Draw filled contour in mask
# [cv2.drawContours(mask, [contours[i]], 0, 255, -1) for i in listindex]

cv2.drawContours(mask, [contours[0]], 0, 255, -1)


img = cv2.cvtColor(img, cv2.COLOR_RGB2RGBA)
out = np.zeros_like(img) # Extract out the object and place into output image

out[mask == 255] = img[mask == 255]


# imgsift=cv2.drawKeypoints(out,kp1,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


if(len(sys.argv)>2):
	cv2.imwrite(sys.argv[2]+'/'+imfile.replace('jpg','png'),out)

else:
	cv2.imshow('Output', out)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

# Show image
# imgcont = img.copy()
# plt.imshow(imgcont),plt.show()
#