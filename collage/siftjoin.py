#!/usr/bin/python3

import numpy as np
import cv2
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import math
import sys
import os

bb1=0
bb2=200
cnt=1

if (len(sys.argv)>1):
    dire=sys.argv[1]
    name=dire
else:
    dire='.'
    name='collage'

filelist=os.listdir(dire)
for fichier in filelist[:]: # filelist[:] makes a copy of filelist.
    if not(fichier.endswith(".png")):
        filelist.remove(fichier)

if (len(sys.argv)>2):
    imfile1=(sys.argv[2])
else:
    imfile1=filelist[0]
    filelist.remove(imfile1)

print(filelist)
print(imfile1)

img1 = cv2.imread(dire+'/'+imfile1, cv2.IMREAD_UNCHANGED)
img1 = cv2.copyMakeBorder(img1, bb1, bb1, bb1, bb1, cv2.BORDER_CONSTANT, value=[0,0,0,0])
gray1 = cv2.cvtColor(img1, cv2.COLOR_RGBA2GRAY)

sift = cv2.xfeatures2d.SIFT_create()

kp1, des1 = sift.detectAndCompute(gray1,None)

for filez in filelist:

    gray1 = cv2.cvtColor(img1, cv2.COLOR_RGBA2GRAY)

    img2 = cv2.imread(dire+'/'+filez, cv2.IMREAD_UNCHANGED)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_RGBA2GRAY)

    # find the keypoints and descriptors with SIFT
    kp2, des2 = sift.detectAndCompute(gray2,None)

    bf = cv2.BFMatcher()
    try:
        matches = bf.knnMatch(des1,des2,k=2)
        # Apply ratio test
        # matches = sorted(matches, key = lambda x:x.distance)

        good = []
        for m,n in matches:
            if m.distance < 0.75*n.distance:
                good.append([m])

        imgsift = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        cv2.imshow('Output', img1)
        cv2.waitKey(0)

        kpimg1=good[0][0].queryIdx
        kpimg2=good[0][0].trainIdx

        size1=kp1[kpimg1].size
        angle1=kp1[kpimg1].angle
        xx1,yy1,=kp1[kpimg1].pt

        size2=kp2[kpimg2].size
        angle2=kp2[kpimg2].angle
        xx2,yy2,=kp2[kpimg2].pt


        # scale=size1/size2
        scale = 1
        # print(scale)
        angle=angle1-angle2
        # print(angle1,angle2,angle)
        center=(xx2+bb2,yy2+bb2)
        # print(center)

        (hh, ww) = img2.shape[:2] 
        hh=hh+2*bb2
        ww=ww+2*bb2
        M = cv2.getRotationMatrix2D(center, -angle, scale) 

        img2 = cv2.copyMakeBorder(img2, bb2, bb2, bb2, bb2, cv2.BORDER_CONSTANT, value=[0,0,0,0])
        img2 = cv2.warpAffine(img2, M, (hh, ww)) 

        # imgsift = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        x_offset=int((xx1)-(xx2+bb2))
        y_offset=int((yy1)-(yy2+bb2))
        # img1[y_offset:y_offset+img2.shape[0], x_offset:x_offset+img2.shape[1]] = img2
        (hh1, ww1) = img1.shape[:2] 

        y1, y2 = y_offset, y_offset + img2.shape[0]
        x1, x2 = x_offset, x_offset + img2.shape[1]


        if (y1 < 0):
            y1 = 0
            ny1 = -y_offset
        else:
            ny1 = 0

        if (y2 > hh1):
            y2=hh1
            ny2=hh1-y_offset
        else:
            ny2 = img2.shape[0]

        if (x1 < 0):
            x1=0
            nx1 = -x_offset
        else:
            nx1 = 0

        if(x2 > ww1):
            x2=ww1
            nx2=ww1-x_offset
        else:
            nx2 = img2.shape[1]


        alpha_s = img2[ny1:ny2, nx1:nx2, 3] / 255.0
        alpha_l = 1.0 - alpha_s

        print(y1,y2,x1,x2)
        print(ny1,ny2,nx1,nx2)


        for c in range(0, 3):
            img1[y1:y2, x1:x2, c] = (alpha_s * img2[ny1:ny2, nx1:nx2, c] + alpha_l * img1[y1:y2, x1:x2, c])


        # imgsift1=cv2.drawKeypoints(img,kp1,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # imgsift2=cv2.drawKeypoints(img2,kp2,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # imstack1 = cv2.resize(imgsift1,(500,400))
        # imstack2 = cv2.resize(imgsift2,(500,400))
        # imgsift = np.hstack((imstack1, imstack2))


        newimg=cv2.cvtColor(img1, cv2.COLOR_RGBA2RGB)
        cnt=cnt+1
        # cv2.waitKey(0)
        cv2.destroyAllWindows()
    except:
        pass
cv2.imwrite(name+'.png',newimg)


    # Show image
    # imgcont = img.copy()
    # plt.imshow(imgcont),plt.show()
    #