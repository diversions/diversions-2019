Diversions 2019
==================

<https://diversions.constantvzw.org/wiki/>

Collage
-------

Requirements for the collage script:
* numpy 
* matplotlib
* opencv-python==3.4.2.17
* opencv-contrib-python=3.4.2.17

(specific versions of opencv as SIFT became non-free afterwards)
