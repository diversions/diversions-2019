import json, csv, sys, argparse

ap = argparse.ArgumentParser("Convert RDF-JSON to either csv or simplified JSON")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
ap.add_argument("--json", action="store_true", default=False, help="output simplififed json")
args = ap.parse_args()
data = json.load(args.input)
rows = [{key: value['value'] for key, value in x.items()} for x in data['results']['bindings']]
if args.json:
    print (json.dumps(rows, indent=2))
else:
    import csv
    keys = data['head']['vars']
    out = csv.DictWriter(args.output, keys)
    out.writeheader()
    for item in rows:
        # print (item)
        out.writerow(item)

