rdf_json_to_csv.py
======================

Convert [RDF-JSON](https://jena.apache.org/documentation/io/rdf-json.html) to either CSV or a simplified (flatter) JSON structure.

Usage:

    python3 rdf_json_to_csv.py < data.json > data.csv

or

    python3 rdf_json_to_csv.py --json < data.json > data.simple.json
