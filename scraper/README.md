
scrape.py
==============

Selenium is a way to use python to automate a browser and is useful to automatically collect and download an information.


Installing the selenium driver
-----------------------------------

<http://selenium-python.readthedocs.io/>


Install necessary python libraries to run these scripts
----------------------------------------------------------
Python 3 with:

    pip3 install selenium requests



Adding the scraper folder to the PATH
----------------------------------------------
Make sure you are in the scraper folder.
Add the current directory to the PATH
```
cd /path/to/scraper
```

Then add the current path to the PATH variable.
```
export PATH=`pwd`:$PATH
```

You can also achieve the above with:

    source changepath.sh



This is a temporary change, when you open a new terminal (or restart) you need to do it once again.


csv2html.py
==========================

Needs
----------
    pip3 install jinja2

Usage
----------------

    python csv2html.py template.html < scrape.csv > scrape.html
