import csv
#!/usr/bin/env python3

import argparse, sys, json, time, os
from jinja2 import Template, DictLoader, Environment, FileSystemLoader

ap = argparse.ArgumentParser("jinjafy")
ap.add_argument("template")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
args = ap.parse_args()

from csv import DictReader

data = list(DictReader(args.input))

tpath, tname = os.path.split(args.template)
env = Environment(loader=FileSystemLoader(tpath))

try:
    import jinjafilters
    for name, fn in jinjafilters.all.items():
        env.filters[name] = fn
except ImportError:
    pass

template = env.get_template(tname)
if type(data) == list:
    # print ("Detected list, adding as {0}".format(args.listkey), file=sys.stderr)
    data = {
        'rows': data
    }
print (template.render(**data), file=args.output)
